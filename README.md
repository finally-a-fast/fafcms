[![Finally a fast CMS](https://assets.gitlab-static.net/uploads/-/system/group/avatar/3119903/logo.png?width=80)](https://www.finally-a-fast.com/) Finally a fast CMS
================================================

[![Latest Stable Version](https://poser.pugx.org/finally-a-fast/fafcms/version)](https://packagist.org/packages/finally-a-fast/fafcms)
[![Total Downloads](https://poser.pugx.org/finally-a-fast/fafcms/downloads)](https://packagist.org/packages/finally-a-fast/fafcms)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/finally-a-fast/fafcms/license)](https://packagist.org/packages/finally-a-fast/fafcms)


This is the project template for the Finally a fast CMS.

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

```
php composer.phar create-project --prefer-dist --stability=dev finally-a-fast/fafcms fafcms
```

Production
----------

1. In ``config/overwrite.php`` set
   - ``YII_DEBUG`` to ``false``
   - ``YII_ENV`` to ``prod``
2. Run ``composer compress``



Documentation
------------

[Documentation](https://www.finally-a-fast.com/) will be added soon at https://www.finally-a-fast.com/.

License
-------

**fafcms** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
