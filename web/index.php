<?php
define('FAFCMS_BASE_PATH', dirname(__DIR__));
define('FAFCMS_VENDOR_PATH', FAFCMS_BASE_PATH . '/vendor');
define('FAFCMS_CORE_PATH', FAFCMS_VENDOR_PATH . '/finally-a-fast/fafcms-core/src');

require(FAFCMS_CORE_PATH . '/web.php');
