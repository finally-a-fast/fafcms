<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class FrontendAsset
 * @package app\assets
 */
class FrontendAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/assets/files';

    /**
     * @var array
     */
    public $js = [];

    /**
     * @var array
     */
    public $css = [];

    /**
     * @var array
     */
    public $depends = [];
}
